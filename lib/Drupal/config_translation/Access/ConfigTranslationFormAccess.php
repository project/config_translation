<?php

/**
 * @file
 * Contains \Drupal\config_translation\Access\ConfigNameCheck.
 */

namespace Drupal\config_translation\Access;

use Drupal\config_translation\ConfigMapperManagerInterface;
use Drupal\Core\Access\StaticAccessCheckInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

/**
 * Checks access for displaying the translation add, edit, and delete forms.
 */
class ConfigTranslationFormAccess extends ConfigTranslationOverviewAccess {

  /**
   * {@inheritdoc}
   */
  public function appliesTo() {
    return array('_config_translation_form_access');
  }

  /**
   * {@inheritdoc}
   */
  public function access(Route $route, Request $request, AccountInterface $account) {
    // For the translation forms we have a target language, so we need some
    // checks in addition to the checks performed for the translation overview.
    $base_access = parent::access($route, $request, $account);
    if ($base_access === static::ALLOW) {
      /** @var \Drupal\config_translation\ConfigMapperInterface $mapper */
      $mapper = $this->configMapperManager->createInstance($route->getDefault('plugin_id'));
      $mapper->populateFromRequest($request);

      $source_language = $mapper->getLanguageWithFallback();
      $target_language = language_load($request->attributes->get('langcode'));

      // Make sure that the target language is not locked, and that the target
      // language is not the original submission language. Although technically
      // configuration can be overlaid with translations in the same language,
      // that is logically not a good idea.
      $access =
        !empty($target_language) &&
        !$target_language->locked &&
        $target_language->id != $source_language->id;

      return $access ? static::ALLOW : static::DENY;
    }
    return static::DENY;
  }

}
